# Computer Craft: Tweaked OS (CCCTOS)
Cameron's custom [CC: Tweaked](https://tweaked.cc/) Operating System

## Installation

- Open a Computer or Turtle in game
- Run the command `lua` to enter the Lua shell
- Copy and paste the following command: `file = fs.open("dl", "w"); file.write(http.get("https://gitlab.com/jafacakes2011/computercraft-tweaked-os/-/raw/master/install.lua").readAll()); file.close(); shell.run("dl"); os.reboot()`

## Websocket

To run the websocket simply run `yarn dev`.
