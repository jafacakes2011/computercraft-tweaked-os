import { Server } from "ws";
import WebSocket = require("ws");
import { Account, Card } from "./models";


const wss = new Server({ port: 5757});

async function newAccount(name: string) {
    return Account.create({ name, balance: 0 });
}

async function getAccount(accountId: number) {
    return Account.findByPk(accountId);
}

async function updateAccount(accountData: any) {
    const account = await Account.findByPk(accountData.id);
    if (account) {
        account.name = accountData.name
        account.balance = accountData.balance
        await account.save()
    }
    return account
}

async function getAccountForCard(cardData: any) {
    console.log("Getting Card");
    console.log(cardData);
    const card = await Card.findByPk(cardData.id)
    console.log("Got Card");
    console.log(card);
    if (!card) {
        return null
    }
    console.log(card.uuid);
    console.log(cardData.uuid);
    if (card.uuid !== cardData.uuid) {
        return null
    }
    return Account.findByPk(card.accountId)
}

async function newCard(accountId: number) {
    return Card.create({ accountId });
}

function isEmpty(obj: any) {
    return Object.keys(obj).length === 0;
}

async function casinoBank(ws: WebSocket, command: any) {
    const responseObj: any = {}
    if (command.command === "new_account") {
        responseObj.account = await newAccount(command.name);
    }
    if (command.command === "get_account") {
        responseObj.account = await getAccount(command.account_id);
    }
    if (command.command === "update_account") {
        responseObj.account = await updateAccount(command.account);
    }
    if (command.command === "get_account_from_card") {
        responseObj.account = await getAccountForCard(command.card);
    }
    if (command.command === "new_card") {
        responseObj.card = await newCard(command.account_id);
    }
    if (!isEmpty(responseObj)) {
        const response = JSON.stringify(responseObj);
        console.log("Returning...");
        console.log(response);
        ws.send(response);
    }
}

async function casinoRoulette(ws: WebSocket, command: any) {
    const responseObj: any = {}
    if (command.command === "roulette_table_spinning") {
        // Wait for 2min (120 sewconds)
        // await new Promise(r => setTimeout(r, 120 * 1000));
        // Lock Table
        // Wait for 30 seconds
        // await new Promise(r => setTimeout(r, 30 * 1000));
        // Return the winning number
        responseObj.winning_number = Math.floor(Math.random() * 37);
    }
    if (command.command === "roulette_table_stopped") {
        // Calculate winnings and update balances
        // Inform Boards to reset
        responseObj.stopped = true;
    }
    if (command.command === "submit_roulette") {
        // command.card.id
        // command.card.uuid
        // command.bets {
            // NUMBER/BLACK/WHITE: AMOUNT,
            // NUMBER/BLACK/WHITE: AMOUNT,
            // NUMBER/BLACK/WHITE: AMOUNT,
        // }
        responseObj.submitted = true;
    }
    if (!isEmpty(responseObj)) {
        const response = JSON.stringify(responseObj);
        console.log("Returning...");
        console.log(response);
        ws.send(response);
    }
}

wss.on("connection", async (ws) => {
    ws.on("message", async (data) => {
        const command = JSON.parse(data.toString());
        console.log("Command");
        console.log(command.command);
        await casinoBank(ws, command)
        await casinoRoulette(ws, command)
    })
})
