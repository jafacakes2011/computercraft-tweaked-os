import { Sequelize } from 'sequelize-typescript';
import { DataType } from 'sequelize-typescript';
import { Table, Column, Model, HasMany, ForeignKey , BelongsTo } from 'sequelize-typescript'

const sequelize = new Sequelize({
  dialect: 'sqlite',
  storage: 'database.sqlite'
});
sequelize.authenticate();

@Table
export class Account extends Model {
  @Column
  name!: string

  @Column({ type: DataType.INTEGER, allowNull: false })
  balance!: Date

  @HasMany(() => Card)
  cards!: Card[]
}

@Table
export class Card extends Model {
  @Column({ type: DataType.UUID, defaultValue: DataType.UUIDV4, allowNull: false })
  uuid!: string;

  @ForeignKey(() => Account)
  @Column
  accountId!: number

  @BelongsTo(() => Account)
  account!: Account
}

sequelize.addModels([Account, Card]);
sequelize.sync({ alter: true });