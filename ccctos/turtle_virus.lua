local turtle_virus = {}

local needed_items = {
    "coal",
    "cobblestone",
    "diamond",
    "stone",
    "redstone",
    "wood",
}

function turtle_virus.startup()
    print("Virus Enabled!")
    turtle.turnLeft()
    turtle.turnLeft()
    turtle.turnLeft()
    turtle.turnLeft()
    shell.run("reboot")
end

-- Collect resources
-- Smelt sand into glass
-- Craft glass panes
-- Smelt cobblestone into stone
-- Craft Computer
-- Craft Sticks
-- Craft Diamond Pickaxe
-- Craft mining turtle
-- Label turtle "turtle_virus_ID"
-- Download CCCTOS onto turtle
-- Reboot turtle

return turtle_virus
