local gps_base = {}

local coords_file_path = "coords.txt"

function gps_base.GetCoordinates()
    if fs.exists(coords_file_path) then
        local coords_file = fs.open(coords_file_path, "r")
        local x = coords_file.readLine()
        local y = coords_file.readLine()
        local z = coords_file.readLine()
        coords_file.close()
        return x, y, z
    else
        local coords_file = fs.open(coords_file_path, "w")
        print("Please provide us with the coordinates of this gps station:")
        print("X:")
        local x = read()
        coords_file.writeLine(x)
        print("Y:")
        local y = read()
        coords_file.writeLine(y)
        print("Z:")
        local z = read()
        coords_file.writeLine(z)
        coords_file.close()
        return x, y, z
    end

end


function gps_base.Startup()
    print(shell.run("id"))
    shell.run("gps host -1039 501 252")
end


return gps_base
