local base = {}

function base.Startup()
    local computer_label = os.getComputerLabel()
    if computer_label and string.find(computer_label, "entrance") then
        local banner = require "ccctos/casino/banner"
        banner.Startup("entrance")
    end
    if computer_label and string.find(computer_label, "racer") then
        local racer = require "ccctos/casino/racer"
        racer.Startup()
    end
    if computer_label and string.find(computer_label, "admin") then
        local bank = require "ccctos/casino/admin"
        bank.Startup()
    end
    if computer_label and string.find(computer_label, "cashier") then
        local cashier = require "ccctos/casino/cashier"
        cashier.Startup()
    end
    if computer_label and string.find(computer_label, "roulette") then
        local roulette = require "ccctos/casino/roulette"
        roulette.Startup()
    end
end

return base
