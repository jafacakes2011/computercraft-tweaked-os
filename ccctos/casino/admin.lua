local server = require "ccctos/casino/server"

local admin = {}

local disk_filename = "data.txt"


function admin.Startup()
    while true do
        print("Enter command:")
        local command = read()
        if command == "new_account" then
            admin.newAccount()
        end
        if command == "new_card" then
            admin.newCard()
        end
        if command == "balance" then
            admin.getBalance()
        end
        if command == "card_info" then
            admin.getCard()
        end
        print("Complete!")
    end
end


function admin.newAccount()
    print("Account Name:")
    local name = read()
    local account = server.account.new(name)
    print("Account " .. account["id"] .. " created!")
    return account
end


function admin.getAccount()
    print("Existing Account ID:")
    local account_id = read()
    return server.account.get(account_id)
end


function admin.getOrCreateAccount()
    print("New Account? (y/n)")
    local response = read()
    local account = {}
    if response == "n" then
        account = admin.getAccount()
    else
        account = admin.newAccount()
    end
    return account
end


function admin.getBalance()
    local account = admin.getOrCreateAccount()
    print("Balance: ")
    print(account["balance"])
end


function admin.newCard()
    local account = admin.getOrCreateAccount()
    local card = server.card.new(account["id"])
    admin.writeCardToDisk(card)
    disk.setLabel("bottom", account["name"] .. "'s Membership Card")
end


function admin.getCard()
    local card = admin.loadCardFromDisk()
    print("Card")
    print("ID:")
    print(card["id"])
    print("UUID:")
    print(card["uuid"])
    local account = server.account.getFromCard(card)
    print("Account")
    print("ID:")
    print(account["id"])
    print("Name:")
    print(account["name"])
    print("Balance:")
    print(account["balance"])
end


function admin.loadCardFromDisk()
    local card = {}
    -- Check a disk is in the drive
    if not disk.isPresent("bottom") then
        print("No disk in Drive...")
        os.sleep(2)
        return admin.loadCardFromDisk()
    end
    local disk_path = disk.getMountPath("bottom")
    local disk_data = fs.open(disk_path .. "/" .. disk_filename, "r")
    -- Read from disk
    card["id"] = tonumber(disk_data.readLine())
    card["uuid"] = disk_data.readLine()
    disk_data.close()
    local account = server.account.getFromCard(card)
    card["account_id"] = account["id"]
    return card
end


function admin.writeCardToDisk(card)
    -- Check a disk is in the drive
    if not disk.isPresent("bottom") then
        print("No disk in Drive...")
        os.sleep(2)
        admin.writeCardToDisk(card)
    else
        local disk_path = disk.getMountPath("bottom")
        local disk_data = fs.open(disk_path .. "/" .. disk_filename, "w")
        -- Write to disk
        disk_data.writeLine(card["id"])
        disk_data.writeLine(card["uuid"])
        disk_data.close()
    end
end


return admin
