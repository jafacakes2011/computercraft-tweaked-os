local racer = {}

local track_length = 7

function racer.Startup(id)
    -- Refuel
    turtle.refuel()
    -- Wait for start command
    -- TODO: Write command
    if false then
        -- Start race
        racer.Start()
        -- Go back to start
        racer.Reset()
    end
    -- Reboot
    -- os.reboot()
end

function racer.Start()
    for i = 1, track_length, 1 do
        turtle.forward()
        print("Forward")
    end
end

function racer.Reset()
    for i = 1, track_length, 1 do
        turtle.back()
        print("Back")
    end
end

return racer
