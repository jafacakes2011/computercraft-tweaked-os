local roulette = {}
local server = require "ccctos/casino/server"

local monitor = peripheral.find("monitor")

roulette["buttons"] = {
    {
        number=0,
        colour=1,
        x=4,
        y=1,
    },
    {
        number=1,
        colour=16384,
        x=1,
        y=2,
    },
    {
        number=2,
        colour=32768,
        x=4,
        y=2,
    },
    {
        number=3,
        colour=16384,
        x=7,
        y=2,
    },
    {
        number=4,
        colour=32768,
        x=1,
        y=3,
    },
    {
        number=5,
        colour=16384,
        x=4,
        y=3,
    },
    {
        number=6,
        colour=32768,
        x=7,
        y=3,
    },
    {
        number=7,
        colour=16384,
        x=1,
        y=4,
    },
    {
        number=8,
        colour=32768,
        x=4,
        y=4,
    },
    {
        number=9,
        colour=16384,
        x=7,
        y=4,
    },
    {
        number=10,
        colour=32768,
        x=1,
        y=5,
    },
    {
        number=11,
        colour=32768,
        x=4,
        y=5,
    },
    {
        number=12,
        colour=16384,
        x=7,
        y=5,
    },
    {
        number=13,
        colour=32768,
        x=1,
        y=6,
    },
    {
        number=14,
        colour=16384,
        x=4,
        y=6,
    },
    {
        number=15,
        colour=32768,
        x=7,
        y=6,
    },
    {
        number=16,
        colour=16384,
        x=1,
        y=7,
    },
    {
        number=17,
        colour=32768,
        x=4,
        y=7,
    },
    {
        number=18,
        colour=16384,
        x=7,
        y=7,
    },
    {
        number=19,
        colour=16384,
        x=1,
        y=8,
    },
    {
        number=20,
        colour=32768,
        x=4,
        y=8,
    },
    {
        number=21,
        colour=16384,
        x=7,
        y=8,
    },
    {
        number=22,
        colour=32768,
        x=1,
        y=9,
    },
    {
        number=23,
        colour=16384,
        x=4,
        y=9,
    },
    {
        number=24,
        colour=32768,
        x=7,
        y=9,
    },
    {
        number=25,
        colour=16384,
        x=1,
        y=10,
    },
    {
        number=26,
        colour=32768,
        x=4,
        y=10,
    },
    {
        number=27,
        colour=16384,
        x=7,
        y=10,
    },
    {
        number=28,
        colour=32768,
        x=1,
        y=11,
    },
    {
        number=29,
        colour=32768,
        x=4,
        y=11,
    },
    {
        number=30,
        colour=16384,
        x=7,
        y=11,
    },
    {
        number=31,
        colour=32768,
        x=1,
        y=12,
    },
    {
        number=32,
        colour=16384,
        x=4,
        y=12,
    },
    {
        number=33,
        colour=32768,
        x=7,
        y=12,
    },
    {
        number=34,
        colour=16384,
        x=1,
        y=13,
    },
    {
        number=35,
        colour=32768,
        x=4,
        y=13,
    },
    {
        number=36,
        colour=16384,
        x=7,
        y=13,
    }
}

function roulette.Startup()
    local computer_label = os.getComputerLabel()
    if computer_label and string.find(computer_label, "table") then
        roulette.Table()
    end
    if computer_label and string.find(computer_label, "board") then
        roulette.Board()
    end
    os.reboot()
end

function roulette.AddRed()
    print("Adding Red")
end

function roulette.AddBlack()
    print("Adding Black")
end

function roulette.AddNumber(number)
    print("Adding " .. number)
end

function roulette.GetNumberFromCoords(x, y)
    local button = nil
    for k, but in pairs(roulette["buttons"]) do
        if y == but.y then
            if x >=but.x and x <= but.x + 1 then
                button = but
                break
            end
        end
    end
    if button ~= nil then
        return button.number
    end
    return nil
end

function roulette.GetButtonFromNumber(number)
    local button = nil
    for k, but in pairs(roulette["buttons"]) do
        if but.number == number then
            button = but
            break
        end
    end
    return button
end

function roulette.DetectButtonPress()
    local event, side, xPos, yPos = os.pullEvent("monitor_touch")
    -- Detect Red/Black
    if (yPos == 14) then
        if (xPos <= 3) then
            roulette.AddRed()
        elseif (xPos > 3 and xPos <= 8) then
            roulette.AddBlack()
        end
    end
    local number = roulette.GetNumberFromCoords(xPos, yPos)
    if number ~= nil then
        roulette.AddNumber(number)
    end
end

function roulette.BuildButton(button)
    monitor.setCursorPos(button.x, button.y)
    monitor.write(tostring(button.number))
end

function roulette.GetColourForNumber(number)
    local button = roulette.GetButtonFromNumber(number)
    return button.colour
end

function roulette.BuildBoard()
    -- Reset monitor
    monitor.clear()
    monitor.setTextScale(1)
    monitor.setBackgroundColor(8192)
    -- Build the number board
    for k, button in pairs(roulette["buttons"]) do
        -- Set Button Colour
        monitor.setTextColour(button.colour)
        roulette.BuildButton(button)
    end
    -- Add Red and Black buttons
    local x, y = monitor.getCursorPos()
    x = 1
    y = y + 1
    monitor.setCursorPos(x, y)
    monitor.setTextColour(16384)
    monitor.write("Red")
    monitor.setTextColour(32768)
    monitor.write("Black")
end


function roulette.Board()
    while true do
        -- Ask for disk
        -- Load Account and get balance
        -- Connect to server
        roulette.BuildBoard()
        while true do
            -- Check if the table is open
            if false then
                break
            end
            roulette.DetectButtonPress()
        end
        -- Update Server with Bets
        -- Wait for the table to be open again
    end
    os.reboot()
end

local CircleOriginX = 25
local CircleOriginY = 18
local CircleRadius = 20
local RadianOffset = 0.17

function roulette.PointOnCircle(count)
    -- degrees to radians
    local radians = RadianOffset * count
    local x = CircleOriginX + CircleRadius * math.cos(radians)
    local y = CircleOriginY + CircleRadius * ( math.sin(radians) * 0.66 )
    return x, y
end

local table = {}

table["offset"] = 0
table["numbers"] = {0, 32, 15, 19, 4, 21, 2, 25, 17, 34, 6, 27, 13, 36, 11, 30, 8, 23, 10, 5, 24, 16, 33, 1, 20, 14, 31, 9, 22, 18, 29, 7, 28, 12, 35, 3, 26}

function table.Spin(winning_number)
    local remaining_spins = 100000
    local wait_time = 0.01
    if winning_number ~= nil then
        remaining_spins = 5
    end
    local stop = false
    while true do
        table["offset"] = 0
        while table["offset"] <= 36 do
            os.sleep(wait_time)
            table.Build()
            if table.IsWinningNumber(winning_number) and remaining_spins == 0 then
                stop = true
                break
            end
            table["offset"] = table["offset"] + 1
        end

        if winning_number ~= nil then
            remaining_spins = remaining_spins - 1
            wait_time = wait_time * 2
        end

        if stop then
            break
        end
    end
end

function table.GetCurrentNumber()
    local reverse_key = 36 - table["offset"] + 1
    for k, number in pairs(table["numbers"]) do
        if k == reverse_key then
            return number
        end
    end
end

function table.IsWinningNumber(number)
    if number == nil then
        return false
    end
    local current_number = table.GetCurrentNumber()
    return number == current_number
end

function table.Build()
    table.Clear()
    local current_count = table["offset"]
    for k, number in pairs(table["numbers"]) do
        local pointX, pointY = roulette.PointOnCircle(current_count)

        monitor.setCursorPos(pointX, pointY)
        local colour = roulette.GetColourForNumber(number)
        monitor.setTextColour(colour)
        monitor.write(tostring(number))
        current_count = current_count + 1
    end
end

function table.Clear()
    monitor.clear()
    monitor.setTextScale(1)
    monitor.setCursorPos(1, 1)
    monitor.setTextColour(1)
    monitor.setBackgroundColor(8192)
end

function table.PrintWinningNumber(winning_number)
    local colour = roulette.GetColourForNumber(winning_number)
    monitor.setCursorPos(CircleOriginX, CircleOriginY)
    monitor.setTextColour(colour)
    monitor.write(tostring(winning_number))
end


function roulette.Table()
    while server.IsOnline() do
        table.Build(0)
        local winning_number = nil
        function Spin()
            table.Spin(nil)
        end
        function Receive()
            winning_number = server.roulette_table.spinning()
        end
        parallel.waitForAny(Spin, Receive)
        print("Winning Number: " .. winning_number)
        table.Spin(winning_number)
        table.PrintWinningNumber(winning_number)
        os.sleep(5)
        server.roulette_table.stopped()
    end
end

return roulette
